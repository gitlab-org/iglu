{
  "$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description": "Schema for a gitlab-experiment events",
  "self": {
    "vendor": "com.gitlab",
    "name": "gitlab_experiment",
    "version": "1-0-2",
    "format": "jsonschema"
  },
  "type": "object",
  "additionalProperties": false,
  "required": ["experiment", "key", "variant"],
  "properties": {
    "experiment": {
      "description": "The name of the experiment",
      "type": "string",
      "maxLength": 255
    },
    "key": {
      "description": "Context key, as generated from the experiment context",
      "type": "string",
      "maxLength": 64
    },
    "variant": {
      "description": "Variant determined to use for the context key",
      "type": "string",
      "maxLength": 255
    },
    "migration_keys": {
      "description": "Optional array of previous context keys",
      "type": ["array", "null"],
      "items": {
        "description": "Previous context key, as generated from experiment context",
        "type": "string",
        "maxLength": 64
      }
    },
    "performance": {
      "description": "Performance metrics based on the experiment decision tree (in Milliseconds)",
      "type": ["object", "null"],
      "additionalProperties": false,
      "properties": {
        "cache_resolution": {
          "description": "Time spent in the cache -- can be a hit or miss based on the presence of other metrics",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "exclusion": {
          "description": "Time in resolving any exclusion callback logic (if present)",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "segmentation": {
          "description": "Time in resolving any runtime segmentation (if present)",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "inclusion": {
          "description": "Time in determining inclusion in the experiment group",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "rollout": {
          "description": "Time in resolving the rollout strategy",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "behavior": {
          "description": "Time in the behavior method or provided block for the assigned variant",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        },
        "total": {
          "description": "Total runtime for the experiment",
          "type": ["number", "null"],
          "minimum": 0,
          "maximum": 64
        }
      }
    }
  }
}
