{
  "$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description": "Standard schema for a gitlab events",
  "self": {
    "vendor": "com.gitlab",
    "name": "gitlab_standard",
    "version": "1-0-7",
    "format": "jsonschema"
  },
  "type": "object",
  "additionalProperties": false,
  "required": ["environment", "source"],
  "properties": {
    "project_id": {
      "description": "ID of the associated project",
      "type": ["integer", "null"],
      "minimum": 0,
      "maximum": 2147483647
    },
    "namespace_id": {
      "description": "ID of the associated namespace",
      "type": ["integer", "null"],
      "minimum": 0,
      "maximum": 2147483647
    },
    "user_id": {
      "description": "ID of the associated user",
      "type": ["integer", "null"],
      "minimum": 0,
      "maximum": 2147483647
    },
    "environment": {
      "description": "Name of the source environment, such as `production` or `staging`",
      "type": "string",
      "maxLength": 32
    },
    "source": {
      "description": "Name of the source application, such as  `gitlab-rails` or `gitlab-javascript`",
      "type": "string",
      "maxLength": 32
    },
    "plan": {
      "description": "Name of the plan, such as  `free`, `bronze`, `silver`, `premium`, `gold` or `ultimate`",
      "type": ["string", "null"],
      "maxLength": 32
    },
    "google_analytics_id": {
      "description": "Google Analytics ID from the marketing site",
      "type": ["string", "null"],
      "maxLength": 32
    },
    "extra": {
      "description": "Any additional data associated with the event, in the form of key-value pairs",
      "type": ["object", "null"]
    }
  }
}
