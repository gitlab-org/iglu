{
  "$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description": "Schema for a CVS event",
  "self": {
    "vendor": "com.gitlab",
    "name": "secure_cvs",
    "version": "1-0-1",
    "format": "jsonschema"
  },
  "type": "object",
  "required": [
    "advisory_id",
    "advisory_xid",
    "source_xid",
    "start_time",
    "end_time"
  ],
  "properties": {
    "advisory_id": {
      "description": "ID of the advisory that is used in the scan",
      "type": "integer",
      "minimum": 0,
      "maximum": 9223372036854775807
    },
    "advisory_xid": {
      "description": "External ID of the advisory that is used in the scan",
      "type": "string",
      "minLength": 1,
      "maxLength": 36
    },
    "source_xid": {
      "description": "Source ID of the advisory that is used in the scan",
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "start_time": {
      "description": "ISO 8601 UTC value with format yyyy-mm-ddThh:mm:ss, representing when the scan started",
      "type": "string",
      "pattern": "^\\d{4}-\\d{2}-\\d{2}T\\d{2}\\:\\d{2}\\:\\d{2}Z$",
      "minLength": 20,
      "maxLength": 20
    },
    "end_time": {
      "description": "ISO 8601 UTC value with format yyyy-mm-ddThh:mm:ss, representing when the scan finished",
      "type": "string",
      "pattern": "^\\d{4}-\\d{2}-\\d{2}T\\d{2}\\:\\d{2}\\:\\d{2}Z$",
      "minLength": 20,
      "maxLength": 20
    },
    "severity": {
      "description": "CVSS vulnerability levels",
      "type": ["string", "null"],
      "enum": ["low", "medium", "high", "critical"]
    },
    "counts": {
      "description": "Counts of models impacted by the scan",
      "type": ["object", "null"],
      "additionalProperties": false,
      "properties": {
        "possibly_affected_projects": {
          "description": "Count of possibly affected projects",
          "type": ["integer", "null"],
          "minimum": 0,
          "maximum": 2147483647
        },
        "known_affected_projects": {
          "description": "Count of known affected projects",
          "type": ["integer", "null"],
          "minimum": 0,
          "maximum": 2147483647
        },
        "possibly_affected_sbom_occurrences": {
          "description": "Count of possibly affected SBOM occurrences",
          "type": ["integer", "null"],
          "minimum": 0,
          "maximum": 2147483647
        },
        "known_affected_sbom_occurrences": {
          "description": "Count of known affected SBOM occurrences",
          "type": ["integer", "null"],
          "minimum": 0,
          "maximum": 2147483647
        },
        "sbom_occurrences_semver_dialects_errors_count": {
          "description": "Count of SBOM occurrences encountering a SemverDialect error",
          "type": ["integer", "null"],
          "minimum": 0,
          "maximum": 2147483647
        }
      }
    }
  }
}
